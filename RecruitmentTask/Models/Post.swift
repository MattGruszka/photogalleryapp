//
//  Post.swift
//  RecruitmentTask
//
//  Created by Gruszkovsky on 15.09.2017.
//  Copyright © 2017 MateuszGruszka. All rights reserved.
//

import UIKit

class Post: NSObject{
    
    var author: String?
    var author_url: String?
    var filename: String?
    var format: String?
    var height: Int = 0
    var width: Int = 0
    var post_url: String?
    var id: Int = 0
    var count: Int = 0
}

