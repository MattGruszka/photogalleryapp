//
//  Counter.swift
//  RecruitmentTask
//
//  Created by Gruszkovsky on 15.09.2017.
//  Copyright © 2017 MateuszGruszka. All rights reserved.
//

import Foundation

class Counter {
    
    var name: String = ""
    var count: Int = 0
}
