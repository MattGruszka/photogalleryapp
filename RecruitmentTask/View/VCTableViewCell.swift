//
//  VCTableViewCell.swift
//  RecruitmentTask
//
//  Created by Gruszkovsky on 15.09.2017.
//  Copyright © 2017 MateuszGruszka. All rights reserved.
//

import UIKit

class VCTableViewCell: UITableViewCell {

    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var authorName: UILabel!
    @IBOutlet weak var idOfImage: UILabel!
    
    let baseURL = "https://unsplash.it/500?image="
    let imageCache = NSCache<NSString, AnyObject>()

    var item: Post? {
        didSet{
            authorName.text = item!.author
            idOfImage.text = "\(item?.count ?? 999)"
            setupPicture()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupPicture(){
        let pictureNSS = NSString(string: baseURL + String((item?.id)!))
        let pictureURL = URL(string: baseURL + String((item?.id)!))
        self.thumbnail.image = UIImage(named: "loading.jpg")

        if let imageFromCache = imageCache.object(forKey: pictureNSS) as? UIImage{
            self.thumbnail.image = imageFromCache
            return
        }
        
        let downloadPicTask = URLSession.shared.dataTask(with: pictureURL!) { (data, response, error) in
            if error != nil {
                print("error")
                return
            } else {
            DispatchQueue.main.async {
                let imageToCache = UIImage(data: data!)
                self.imageCache.setObject(imageToCache!, forKey: pictureNSS)
                self.thumbnail.image = imageToCache
                }
            }
        }
        downloadPicTask.resume()
    }
}
