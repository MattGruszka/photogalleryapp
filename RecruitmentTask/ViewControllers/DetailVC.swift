//
//  DetailVC.swift
//  RecruitmentTask
//
//  Created by Gruszkovsky on 18.09.2017.
//  Copyright © 2017 MateuszGruszka. All rights reserved.
//

import UIKit

class DetailVC: UIViewController, UIScrollViewDelegate, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var horizontalScroll: UIScrollView!
    @IBOutlet weak var imgOut: UIImageView!
    @IBOutlet weak var idOut: UILabel!
    
    let baseURL = "https://unsplash.it/500?image="
    var photoIndex: Int!
    var posts = [Post]()
    var id = Int()
    
    @IBAction func openZoomingController(recognizer: UITapGestureRecognizer) {
        self.performSegue(withIdentifier: "zooming", sender: nil)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let index = photoIndex {
            id = index
        } else { return }
        setLabels(id: id)
        getPicture(id: id)
        }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let id = segue.identifier,
            let zoomedPhotoViewController = segue.destination as? ZoomedPhotoViewController,
            id == "zooming" {
            zoomedPhotoViewController.photoIndex = photoIndex
        }
    }
    
    fileprivate func setLabels(id: Int) {
        let idText = String(posts[self.id].count)
        let auth = posts[id].author
        let size = String(posts[id].height) + " x " + String(posts[id].width)
        idOut.text = idText + "  " + auth! + "  " + size
    }
    
    fileprivate func getPicture(id: Int){
        let pictureURL = URL(string: baseURL + String(id))
        let downloadPicTask = URLSession.shared.dataTask(with: pictureURL!) { (data, response, error) in
            if error != nil {
                print("error")
                return
            } else {
                DispatchQueue.main.async {
                    let image = UIImage(data: data!)
                    self.imgOut.image = image
                    self.reloadInputViews()
                }
            }
        }
        downloadPicTask.resume()
    }
    
}
