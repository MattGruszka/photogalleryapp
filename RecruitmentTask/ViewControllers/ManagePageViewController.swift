//
//  ManagePageViewController.swift
//  RecruitmentTask
//
//  Created by Gruszkovsky on 04.10.2017.
//  Copyright © 2017 MateuszGruszka. All rights reserved.
//

import UIKit

class ManagePageViewController: UIPageViewController {
    var currentIndex: Int! = -1
    var pageCount: Int = -1
    var posts = [Post]()

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        
        if let viewController = viewDetailVController(currentIndex ?? 0) {
            let viewControllers = [viewController]
            
            setViewControllers(
                viewControllers,
                direction: .forward,
                animated: false,
                completion: nil
            )
        }
    }
    
    func viewDetailVController(_ index: Int) -> DetailVC? {
        guard let storyboard = storyboard,
            let page = storyboard.instantiateViewController(withIdentifier: "DetailVC")
                as? DetailVC else {
                    return nil
        }
        page.photoIndex = index
        page.posts = posts
        return page
    }
}

extension ManagePageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController,
                                viewControllerBefore viewController: UIViewController) -> UIViewController? {
            
    if let viewController = viewController as? DetailVC,
        let index = viewController.photoIndex, index > 0 {
                return viewDetailVController(index - 1)
            }
        return nil
    }
        
    func pageViewController(_ pageViewController: UIPageViewController,
                                viewControllerAfter viewController: UIViewController) -> UIViewController? {
            
        if let viewController = viewController as? DetailVC,
            let index = viewController.photoIndex, (index + 1) < pageCount {
        return viewDetailVController(index + 1)
        }
    return nil
    }
}

