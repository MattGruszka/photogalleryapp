//
//  ViewController.swift
//  RecruitmentTask
//
//  Created by Gruszkovsky on 15.09.2017.
//  Copyright © 2017 MateuszGruszka. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    private let baseURL = "https://unsplash.it"
    private let reachability = Reachability()!
    private let counterAuthor = CounterAuthor()

    private var posts = [Post]()
    private var overallPostCount = 0
    private var tableViewCons:[NSLayoutConstraint] = []
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkConnection()
        getDataFromURL()
        setConstraints()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged(note:)), name: .reachabilityChanged, object: reachability)
        do {
            try reachability.startNotifier()
        } catch {
            print("Could not start reachability notifier.")
        }
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (posts.count)
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! VCTableViewCell
        cell.item = posts[indexPath.item]
        return(cell)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "Detail", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "Detail") {
            guard let index = tableView.indexPathForSelectedRow?.row  else { return }
            let managePageViewController = segue.destination as? ManagePageViewController
            managePageViewController?.posts = posts
            managePageViewController?.currentIndex = index
            managePageViewController?.pageCount = overallPostCount
        }
    }
    
    fileprivate func setConstraints() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(tableView)
        let topConstraint = tableView.topAnchor.constraint(equalTo: self.view.topAnchor)
        let bottomConstraint = tableView.topAnchor.constraint(equalTo: self.view.bottomAnchor)
        let leftConstraint = tableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor)
        let rightConstraint = tableView.rightAnchor.constraint(equalTo: self.view.rightAnchor)
        tableViewCons = [topConstraint, bottomConstraint, leftConstraint, rightConstraint]
        NSLayoutConstraint.activate(tableViewCons)
    }
    
    fileprivate func checkConnection() {

        reachability.whenUnreachable = { reachability in
            self.createAlert(title: "Network Alert", message: "Network connection lost.")
        }
    }
    
    fileprivate func createAlert (title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default, handler: {
                (action) in alert.dismiss(animated:true, completion: nil)
            }))
        self.present(alert, animated:true, completion:nil)
    }
    
    @objc func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability

        if (reachability.connection == .none) {
            self.createAlert(title: "Network Alert", message: "Network connection lost.")
        }
    }
    
    fileprivate func getDataFromURL() {
        DispatchQueue.global(qos: .userInitiated).async {
            let url = URL(string: "https://unsplash.it/list")
            let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
                if error != nil {
                    print("ERROR")
                } else {
                    if let content = data {
                        do {
                            let myJson = try JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers) as! [[String: Any]]
                            
                            for element in myJson {
                                let post = Post()
                                post.format = element["format"] as? String
                                post.width = (element["width"] as? Int)!
                                post.author = element["author"] as? String
                                post.author_url = element["author_url"] as? String
                                post.filename = element["filename"] as? String
                                post.height = (element["height"] as? Int)!
                                post.id = element["id"] as! Int
                                post.post_url = element["post_url"] as? String
                                self.posts.append(post)
                                self.overallPostCount += 1
                                DispatchQueue.main.async {
                                    post.count = self.counterAuthor.checkAuthor(author: (element["author"] as? String)!)
                                    self.tableView.reloadData()
                                }
                            }
                        } catch {
                        }
                    }
                }
            }
            task.resume()
        }
    }
    
    
}
