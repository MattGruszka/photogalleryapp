//
//  ZoomPhotoVC.swift
//  RecruitmentTask
//
//  Created by Gruszkovsky on 03.10.2017.
//  Copyright © 2017 MateuszGruszka. All rights reserved.
//

import UIKit

class ZoomedPhotoViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!

    var photoIndex: Int?
    var height = CGFloat()
    var width = CGFloat()
    
    var widthConstraint = NSLayoutConstraint()
    var heightConstraint = NSLayoutConstraint()
    var xConstaint = NSLayoutConstraint()
    var yConstaint = NSLayoutConstraint()
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    
    override func viewDidLoad() {
        width = imageView.frame.width / 2
        height = imageView.frame.height / 2
        setImage()
        self.imageView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(imageView)
        setConstraints()
        setGestures()
    }
    
    fileprivate func setImage(){
        if let photoIndex = photoIndex {
            let pictureURL = URL(string: "https://unsplash.it/500?image=" + String(photoIndex))
            let downloadPicTask = URLSession.shared.dataTask(with: pictureURL!) { (data, response, error) in
                if error != nil {
                    print("error")
                    return
                } else {
                    DispatchQueue.main.async {
                        let image = UIImage(data: data!)
                        self.imageView.image = image
                        self.reloadInputViews()
                    }
                }
            }
            downloadPicTask.resume()
        } else { return }
    }
    
    fileprivate func setGestures() {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(ZoomedPhotoViewController.pan(gesture:)))
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(ZoomedPhotoViewController.pinch(gesture:)))
        
        var gestures:[UIGestureRecognizer] = [panGesture, pinchGesture]
        for gesture in gestures {
            self.imageView.addGestureRecognizer(gesture)
        }
    }
    
    fileprivate func setConstraints() {
        let widthConstraint = NSLayoutConstraint(item: self.imageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: view.frame.width)
        let heightConstraint = NSLayoutConstraint(item: self.imageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: view.frame.height * 0.55)
        let xConstaint = NSLayoutConstraint(item: self.imageView, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1.0, constant: 0)
        let yConstaint = NSLayoutConstraint(item: self.imageView, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1.0, constant: 0)
        NSLayoutConstraint.activate([widthConstraint, heightConstraint, xConstaint, yConstaint])
    }
    
    @objc func pan (gesture: UIPanGestureRecognizer) {
        var translation = gesture.translation(in: self.view)

        if let gestureView = gesture.view {
            gestureView.center = CGPoint(x: gestureView.center.x + translation.x, y: gestureView.center.y + translation.y)
        }
        gesture.setTranslation(CGPoint.zero, in: self.view)
    }
    
    @objc func pinch (gesture: UIPinchGestureRecognizer) {
        
        if (gesture.state == UIGestureRecognizerState.changed ||
            gesture.state == UIGestureRecognizerState.ended) {
            
        if let gestureView = gesture.view {
            gestureView.transform = CGAffineTransform.init(scaleX: gesture.scale, y: gesture.scale)
            }
           
        if (imageView.frame.width < width || imageView.frame.height < height) {
            if let gestureView = gesture.view {
                var scale = gesture.scale
                gestureView.transform = CGAffineTransform.init(scaleX: scale * 2, y: scale * 2)
            }
        }
        }
    }
}

