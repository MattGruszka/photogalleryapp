//
//  CounterAuthor.swift
//  RecruitmentTask
//
//  Created by Gruszkovsky on 06.10.2017.
//  Copyright © 2017 MateuszGruszka. All rights reserved.
//

import Foundation

class CounterAuthor {
    
    var authorCounter = [Counter]()
    
    func checkAuthor(author: String) -> Int {
        var creationFlag = false
        var final = 0
        if authorCounter.isEmpty {
            var person = Counter()
            person.count += 1
            final = person.count
            person.name = author
            authorCounter.append(person)
        } else {
            creationFlag = false
            for var i in (0..<authorCounter.count) {
                if(author == authorCounter[i].name) {
                    creationFlag = true
                    authorCounter[i].count = authorCounter[i].count + 1
                    final = authorCounter[i].count
                    i += 1
                }
            }
            if (creationFlag == false) {
                var person = Counter()
                person.count = person.count + 1
                final = person.count
                person.name = author
                authorCounter.append(person)
            }
        }
        return final
    }
}
